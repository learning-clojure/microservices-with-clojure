# Building Microservices with Clojure examples

Sample code for [Building Microservices with Clojure video course](http://shop.oreilly.com/product/0636920041894.do) from O'Reilly.

It contains 2 sample [Leiningen](https://leiningen.org/) applications build with [Pedestal framework](http://pedestal.io/) using leiningen `pedestal-service` template: `whole-note` and `project-catalog`.

`whole-note` is a base project with very small changes just to introduce to Pedestal's general structure.

`project-catalog` is based on same template as previous project, but it is changed step by step into RESTful application.

 
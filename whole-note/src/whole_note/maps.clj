(ns whole-note.json-and-maps
  :require [clojure.data.json :as json])

(def projmap {
  :sleeping-cat
  {
    :name "Sleeping Cat Project"
    :framework "Pedestal"
    :language "Clojure"
    :repo "https://gitlab.com/luhtonen/sleepingcat"
    }
  :running-dog
  {
    :name "Running Dog Experiment"
    :framework "Grails"
    :language "Groovy"
    :repo "https://gitlab.com/luhtonen/runningdog"
    }
  })

(:sleeping-cat projmap)

(def raw-str
  (json/write-str projmap))

(println raw-str)

(def projmap-again
  (json/print-str raw-str))

(:sleeping-cat projmap-again)

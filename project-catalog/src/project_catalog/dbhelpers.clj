(ns project-catalog.dbhelpers
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [monger.json]))

(defn get-connect-string []
  (or (System/getenv "MONGO_CONNECTION") "mongodb://localhost:27017/project-catalog"))

(defn get-db-project [proj-name]
  (let [connect-string (get-connect-string)
        {:keys [conn db]} (mg/connect-via-uri connect-string)]
    (mc/find-maps db "project-catalog" {:proj-name proj-name})))
